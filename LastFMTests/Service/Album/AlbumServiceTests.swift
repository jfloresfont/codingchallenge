//
//  AlbumServiceTests.swift
//  LastFMTests
//
//  Created by Javier Flores Font on 21/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
import Moya

@testable import LastFM

final class AlbumServiceTests: XCTestCase {
    func testAlbumSearchWithSuccessReturnsAlbums() {
        // Setup
        let albumService: AlbumServiceType = getAlbumServiceWithResponse(statusCode: 200, jsonFileName: "AlbumSearchSuccessResponse")
        let scheduler = TestScheduler(initialClock: 0)

        // Condition
        let result = scheduler.record(source: albumService.albumSearch(key: "ExampleKey"))

        // Result
        let albums = result.events.first?.value.element?.value
        XCTAssertEqual(albums!.count, 50)
        XCTAssertEqual(albums!.first!, Album(name: "Believe",
                                             artist: "Disturbed",
                                             url: "https://www.last.fm/music/Disturbed/Believe",
                                             image: [
                                                AlbumImage(size: "small", urlString: "https://lastfm-img2.akamaized.net/i/u/34s/bca3b80481394e25b03f4fc77c338897.png"),
                                                AlbumImage(size: "medium", urlString: "https://lastfm-img2.akamaized.net/i/u/64s/bca3b80481394e25b03f4fc77c338897.png"),
                                                AlbumImage(size: "large", urlString: "https://lastfm-img2.akamaized.net/i/u/174s/bca3b80481394e25b03f4fc77c338897.png"),
                                                AlbumImage(size: "extralarge", urlString: "https://lastfm-img2.akamaized.net/i/u/300x300/bca3b80481394e25b03f4fc77c338897.png")]))
    }

    func testAlbumSearchWithFailureReturnsError() {
        // Setup
        let albumService: AlbumServiceType = getAlbumServiceWithResponse(statusCode: 10, jsonFileName: "AlbumSearchErrorResponse")
        let scheduler = TestScheduler(initialClock: 0)

        // Condition
        let result = scheduler.record(source: albumService.albumSearch(key: "ExampleKey"))

        // Result
        XCTAssertEqual(result.events.first!.value.element!.error!, ApiError(message: "Invalid API Key", error: 10))
    }

    private func getAlbumServiceWithResponse(statusCode: Int, jsonFileName: String? = nil) -> AlbumServiceType {
        return AlbumService(provider: MoyaProvider<AlbumApi>(endpointClosure: getEndpointClosure(statusCode: statusCode,
                                                                                                 jsonFileName: jsonFileName),
                                                             stubClosure: MoyaProvider.immediatelyStub))
    }
}
