//
//  TextExtension.swift
//  LastFMTests
//
//  Created by Javier Flores Font on 21/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//
import RxSwift
import RxTest
import RxCocoa
import RxFlow
import Result
import Moya

@testable import LastFM

extension MoyaProvider {
    class func endpointMappingWithStatusCodeAndData(statuscode: Int, data: Data) -> ((Target) -> Endpoint) {
        func endpointMapping(target: Target) -> Endpoint {
            return Endpoint(
                url: URL(target: target).absoluteString,
                sampleResponseClosure: { .networkResponse(statuscode, data) },
                method: target.method,
                task: target.task,
                httpHeaderFields: target.headers
            )
        }
        return endpointMapping
    }
}

final class FileReader {
    static func read(fileName: String, withExtension: String) -> Data? {
        let bundle: Bundle = Bundle(for: self)
        let url = bundle.url(forResource: fileName, withExtension: withExtension)!
        do {
            return try Data(contentsOf: url)
        } catch {
            return nil
        }
    }
}

func getEndpointClosure<T: TargetType>(statusCode: Int, jsonFileName: String? = nil) -> ((T) -> Endpoint) {
    let data = jsonFileName != nil ? FileReader.read(fileName: jsonFileName!, withExtension: ".json")! : Data()
    return MoyaProvider<T>.endpointMappingWithStatusCodeAndData(statuscode: statusCode, data: data)
}

extension TestScheduler {
    /**
     Builds testable observer for s specific observable sequence, binds it's results and sets up disposal.

     - parameter source: Observable sequence to observe.
     - returns: Observer that records all events for observable sequence.
     */
    func record<O: ObservableConvertibleType>(source: O) -> TestableObserver<O.E> {
        let observer = createObserver(O.E.self)
        let disposable = source.asObservable().bind(to: observer)
        scheduleAt(100000) {
            disposable.dispose()
        }
        return observer
    }

    func record(stepper: Stepper) -> TestableObserver<DLStep>{
        return record(source: stepper.step.flatMap{step -> Observable<DLStep> in
            if let step = step as? DLStep {
                return .just(step)
            } else {
                return .empty()
            }
        })
    }
}

extension DLStep: AutoEquatable {}
extension Album: AutoEquatable {}
