//
//  DetailViewModelTests.swift
//  LastFMTests
//
//  Created by Javier Flores Font on 22/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
import RxBlocking
import Result

@testable import LastFM

final class DetailViewModelTests: XCTestCase {

    func testItemsDriverWithAlbumInputShouldHaveCorrectTitle() {
        let album = Album(name: "name", artist: "artist", url: "url", image: [])
        let viewModel = detailViewModel(album: album, imageDownloaderGetURLStringResponse: .just(nil))
        let result = try! viewModel.items.toBlocking(timeout: 1.0).last()!

        if case let DetailViewModel.Item.title(value) = result[1] {
            XCTAssertEqual(value, "title".localized + ": " + album.name)
        } else {
            XCTAssert(false)
        }
    }

    func testItemsDriverWithAlbumInputShouldHaveCorrectDescription() {
        let album = Album(name: "name", artist: "artist", url: "url", image: [])
        let viewModel = detailViewModel(album: album, imageDownloaderGetURLStringResponse: .just(nil))
        let result = try! viewModel.items.toBlocking(timeout: 1.0).last()!

        if case let DetailViewModel.Item.description(value) = result[2] {
            XCTAssertEqual(value, "artist".localized + ": " + album.artist)
        } else {
            XCTAssert(false)
        }
    }

    func testItemsDriverWithAlbumInputShouldHaveCorrectURL() {
        let album = Album(name: "name", artist: "artist", url: "url", image: [])
        let viewModel = detailViewModel(album: album, imageDownloaderGetURLStringResponse: .just(nil))
        let result = try! viewModel.items.toBlocking(timeout: 1.0).last()!

        if case let DetailViewModel.Item.url(value) = result[3] {
            XCTAssertEqual(value, album.url)
        } else {
            XCTAssert(false)
        }
    }

    func testItemsDriverWithFailureDownloadImageShouldHavePlaceholderImage() {
        let album = Album(name: "name", artist: "artist", url: "url", image: [])
        let viewModel = detailViewModel(album: album, imageDownloaderGetURLStringResponse: .just(nil))
        let result = try! viewModel.items.toBlocking(timeout: 1.0).last()!

        if case let DetailViewModel.Item.image(image) = result.first! {
            XCTAssertEqual(image!.rawData(), UIImage(named: "placeholder")!.rawData())
        } else {
            XCTAssert(false)
        }
    }

    func testItemsDriverWithSuccessDownloadImageShouldHaveDownloadedImage() {
        let album = Album(name: "name", artist: "artist", url: "url", image: [])
        let viewModel = detailViewModel(album: album, imageDownloaderGetURLStringResponse: .just(UIImage(named: "example")))
        let result = try! viewModel.items.toBlocking(timeout: 1.0).last()!

        if case let DetailViewModel.Item.image(image) = result.first! {
            XCTAssertEqual(image!.rawData(), UIImage(named: "example")!.rawData())
        } else {
            XCTAssert(false)
        }
    }

    private func detailViewModel(album: Album,
                                 imageDownloaderGetURLStringResponse: Observable<UIImage?>) -> DetailViewModel {
        var imageDownloaderMock = ImageDownloaderMock()
        imageDownloaderMock.getURLStringResponse = imageDownloaderGetURLStringResponse

        return DetailViewModel(album: album, imageDownloader: imageDownloaderMock)
    }
}
