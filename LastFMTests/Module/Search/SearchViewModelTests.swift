//
//  SearchViewModelTests.swift
//  LastFMTests
//
//  Created by Javier Flores Font on 21/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
import RxBlocking
import Result

@testable import LastFM

final class SearchViewModelTests: XCTestCase {
    private var disposeBag: DisposeBag!

    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
    }

    func testItemsDriverWhenEmptyInputSearchTextShouldReturnEmptyItemList() {
        testItemsDriverWhenInputSearch(key: "", serverResponse: .just(.success([])), expectedItems: [])
    }

    func testItemsDriverWhenNotEmptyInputSearchTextAndServerSuccessShouldReturnItemList() {
        let albums = [Album(name: "name", artist: "artist", url: "url", image: [])]
        testItemsDriverWhenInputSearch(key: "key", serverResponse: .just(.success(albums)), expectedItems: albums)
    }

    func testItemsDriverWhenNotEmptyInputSearchTextAndServerErrorShouldReturnEmptyItemList() {
        testItemsDriverWhenInputSearch(key: "key", serverResponse: .just(.failure(ApiError.generic)), expectedItems: [])
    }

    func testErrorDriverWhenServerErrorShouldReturnServerErrorMessage() {
        testErrorDriverWhen(serverResponse: .just(.failure(ApiError.generic)), expectedErrorDriver: ApiError.generic.message)
    }

    func testErrorDriverWhenNoServerErrorShouldNotSendEvent() {
        let albums = [Album(name: "name", artist: "artist", url: "url", image: [])]
        testErrorDriverWhen(serverResponse:  .just(.success(albums)), expectedErrorDriver: nil)
    }

    func testSeletedAlbumShouldNavigateToAlbumDetail() {
        let album = Album(name: "name", artist: "artist", url: "url", image: [])
        let mainScheduler = TestScheduler(initialClock: 0)
        let selectedAlbumPublishSubject = PublishSubject<Album>()
        let viewModel = searchViewModel(selectedAlbum: selectedAlbumPublishSubject.asObservable(),
                                        serverResponse: .just(.success([album])),
                                        mainScheduler: mainScheduler)
        viewModel.selectedAlbum.drive().disposed(by: disposeBag)
        let result = mainScheduler.record(stepper: viewModel)

        mainScheduler.scheduleAt(10) {
            selectedAlbumPublishSubject.onNext(album)
        }
        mainScheduler.start()

        let correctResult = [next(10, DLStep.albumDetail(album: album))]
        XCTAssertEqual(result.events, correctResult)
    }

    private func testItemsDriverWhenInputSearch(key: String, serverResponse: Single<Result<[Album], ApiError>>, expectedItems: [Album]) {
        let viewModel = searchViewModel(searchKey: .just(key), serverResponse: serverResponse)
        let items = try! viewModel.items.toBlocking(timeout: 1.0).last()!
        XCTAssertEqual(items, expectedItems)
    }

    private func testErrorDriverWhen(serverResponse: Single<Result<[Album], ApiError>>, expectedErrorDriver: String?) {
        let mainScheduler = TestScheduler(initialClock: 0)
        let searchKeyPublishSubject = PublishSubject<String>()
        let viewModel = searchViewModel(searchKey: searchKeyPublishSubject.asObservable(),
                                        serverResponse: serverResponse,
                                        mainScheduler: mainScheduler)
        let disposeBag = DisposeBag()
        viewModel.items.drive().disposed(by: disposeBag)
        let result = mainScheduler.record(source: viewModel.error)
        mainScheduler.scheduleAt(10) {
            searchKeyPublishSubject.onNext("key")
        }
        mainScheduler.start()

        if let expectedErrorDriver = expectedErrorDriver {
            XCTAssertEqual(result.events, [next(11, expectedErrorDriver)])
        } else {
            XCTAssertEqual(result.events, [])
        }
    }

    private func searchViewModel(searchKey: Observable<String> =  .empty(),
                                 selectedAlbum: Observable<Album> = .empty(),
                                 serverResponse: Single<Result<[Album], ApiError>>,
                                 mainScheduler: TestScheduler = TestScheduler(initialClock: 0),
                                 backgroundScheduler: TestScheduler = TestScheduler(initialClock: 0)) -> SearchViewModel {
        let albumServiceMock = AlbumServiceMock(albumSearchReturnValue: serverResponse)
        let schedulerProviderMock = SchedulerProviderMock(mainScheduler: mainScheduler,
                                                          backgroundScheduler: backgroundScheduler)
        return SearchViewModel(input: (searchKey: searchKey,
                                       selectedAlbum: selectedAlbum),
                               dependency: (albumService: albumServiceMock,
                                            schedulerProvider: schedulerProviderMock))
    }
}
