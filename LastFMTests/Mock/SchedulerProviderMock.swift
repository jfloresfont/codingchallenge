import RxSwift
import Result
import Moya

@testable import LastFM

final class SchedulerProviderMock: SchedulerProtocol {
    var mainScheduler: SchedulerType
    var backgroundScheduler: SchedulerType

    init(mainScheduler: SchedulerType, backgroundScheduler: SchedulerType) {
        self.mainScheduler = mainScheduler
        self.backgroundScheduler = backgroundScheduler
    }
}
