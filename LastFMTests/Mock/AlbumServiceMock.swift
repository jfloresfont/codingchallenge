//
//  AlbumServiceMock.swift
//  LastFMTests
//
//  Created by Javier Flores Font on 21/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import Foundation
import RxSwift
import Result

@testable import LastFM

struct AlbumServiceMock: AlbumServiceType {
    var albumSearchReturnValue: Single<Result<[Album], ApiError>>

    func albumSearch(key: String) -> Single<Result<[Album], ApiError>> {
        return albumSearchReturnValue
    }
}
