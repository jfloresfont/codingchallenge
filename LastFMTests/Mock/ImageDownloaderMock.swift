//
//  ImageDownloaderMock.swift
//  LastFMTests
//
//  Created by Javier Flores Font on 22/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import Foundation
import RxSwift

@testable import LastFM

struct ImageDownloaderMock: ImageDownloaderType {
    var getURLStringResponse: Observable<UIImage?>!

    func get(urlString: String?) -> Observable<UIImage?> {
       return getURLStringResponse
    }
}
