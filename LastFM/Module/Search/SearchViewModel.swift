//
//  SearchViewModel.swift
//  LastFM
//
//  Created by Javier Flores Font on 21/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Result
import RxFlow

final class SearchViewModel: Stepper {
    private(set) var items: Driver<[Album]>!
    private(set) var error: Driver<String>!
    private(set) var selectedAlbum: Driver<Void>!

    private let errorPublishSubject = PublishSubject<String>()

    init(input: (searchKey: Observable<String>,
                 selectedAlbum: Observable<Album>),
         dependency: (albumService: AlbumServiceType,
                      schedulerProvider: SchedulerProtocol)) {

        items = input.searchKey
            .debounce(0.5, scheduler: dependency.schedulerProvider.mainScheduler)
            .flatMapLatest { key -> Single<[Album]> in
                if key.isEmpty {
                    return .just([])
                } else {
                    return dependency.albumService.albumSearch(key: key).flatMap { [unowned self] result in
                        if let apiError = result.error {
                            self.errorPublishSubject.onNext(apiError.message)
                        }
                        return .just(result.value ?? [])
                    }
                }
            }.asDriver(onErrorJustReturn: [])

        selectedAlbum = input.selectedAlbum.map { [unowned self] album in
            self.step.accept(DLStep.albumDetail(album: album))
        }.asDriver(onErrorJustReturn: ())

        error = errorPublishSubject.asDriver(onErrorJustReturn: "").debug()
    }
}
