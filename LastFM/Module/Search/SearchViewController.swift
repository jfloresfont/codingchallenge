//
//  SearchViewController.swift
//  LastFM
//
//  Created by Javier Flores Font on 21/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

final class SearchViewController: UIViewController, BindableType {
    var viewModel: SearchViewModel!
    var searchKey: Observable<String> {
        return searchKeyPublishSubject.asObservable()
    }
    var selectedAlbum: Observable<Album> {
        return selectedAlbumPublishSubject.asObservable()
    }

    private let searchKeyPublishSubject = PublishSubject<String>()
    private let selectedAlbumPublishSubject = PublishSubject<Album>()

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 70
        }
    }

    @IBOutlet private weak var searchBar: UISearchBar! {
        didSet {
            searchBar.placeholder = "album.name".localized
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "albums".localized
    }

    func bindViewModel() {
        tableView.rx.modelSelected(Album.self).bind(to: selectedAlbumPublishSubject).disposed(by: disposeBag)
        searchBar.rx.text.orEmpty.bind(to: searchKeyPublishSubject).disposed(by: disposeBag)
        viewModel.items.drive(tableView.rx.items(cellIdentifier: LabelTableViewCell.identifier, cellType: LabelTableViewCell.self)) { _, model, cell in
            cell.set(title: model.name)
        }.disposed(by: disposeBag)
        viewModel.error.drive(onNext: { [unowned self] errorString in
            self.alert("error".localized, message: errorString)
        }).disposed(by: disposeBag)
        viewModel.selectedAlbum.drive().disposed(by: disposeBag)
    }
}
