//
//  SearchViewModel.swift
//  LastFM
//
//  Created by Javier Flores Font on 21/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Result
import RxFlow

final class DetailViewModel: Stepper {
    let items: Driver<[Item]>

    init(album: Album, imageDownloader: ImageDownloaderType) {
       self.items = imageDownloader.get(urlString: album.largeSizeImageURLString)
        .startWith(UIImage(named: "placeholder"))
        .filter { $0 != nil }
        .map { image -> [Item] in
            return [.image(image: image),
                    .title(value: "title".localized + ": " + album.name),
                    .description(value: "artist".localized + ": " + album.artist),
                    .url(value: album.url)]
        }.asDriver(onErrorJustReturn: [])
    }

    enum Item {
        case title(value: String)
        case description(value: String)
        case image(image: UIImage?)
        case url(value: String)
    }
}
