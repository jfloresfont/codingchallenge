//
//  DetailViewController.swift
//  LastFM
//
//  Created by Javier Flores Font on 22/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import UIKit

final class DetailViewController: UIViewController, BindableType {
    var viewModel: DetailViewModel!

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 50
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "detail".localized
    }

    func bindViewModel() {
        viewModel.items.drive(tableView.rx.items) { [weak self] tableView, index, item in
            guard let strongSelf = self else { return UITableViewCell() }
            switch item {
            case let .title(value), let .description(value):
                let cell = strongSelf.tableView.dequeueReusableCell(withIdentifier: LabelTableViewCell.identifier) as! LabelTableViewCell
                cell.set(title: value)
                return cell
            case let .url(value):
                let cell = strongSelf.tableView.dequeueReusableCell(withIdentifier: TextViewTableViewCell.identifier) as! TextViewTableViewCell
                cell.set(title: value)
                return cell
            case let .image(image):
                let cell = strongSelf.tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.identifier) as! ImageTableViewCell
                cell.set(image: image)
                return cell
            }
        }.disposed(by: disposeBag)
    }
}
