import Swinject

extension Container {
    static let sharedContainer: Container = {
        let container = Container()
        container.register(AlbumServiceType.self) { _ in AlbumService() }.inObjectScope(.container)
        container.register(ImageDownloaderType.self) { _ in ImageDownloader()}.inObjectScope(.container)
        container.register(SchedulerProtocol.self) { _ in SchedulerProvider()}.inObjectScope(.container)
        return container
    }()
}

