import RxSwift
import Result
import Moya

enum AlbumApi {
    case albumSearch(key: String)
}

protocol AlbumServiceType {
    func albumSearch(key: String) -> Single<Result<[Album], ApiError>>
}

struct AlbumService: AlbumServiceType {
    private let provider: MoyaProvider<AlbumApi>

    init(provider: MoyaProvider<AlbumApi> = MoyaProvider<AlbumApi>()) {
        self.provider = provider
    }

    func albumSearch(key: String) -> Single<Result<[Album], ApiError>> {
        return provider.rx.request(.albumSearch(key: key))
            .map { response -> Result<[Album], ApiError> in
                if response.statusCode == 200 {
                    return try .success(response.map([Album].self, atKeyPath: "results.albummatches.album", using: JSONDecoder(), failsOnEmptyData: true))
                } else {
                    return try .failure(response.map(ApiError.self))
                }
            }.catchErrorJustReturn(.failure(ApiError.generic))
    }
}
