import Moya

extension AlbumApi: TargetType {
    var baseURL: URL {
        return URL(string: "http://ws.audioscrobbler.com")!
    }

    var method: Moya.Method {
        switch self {
        case .albumSearch:
            return .get
        }
    }

    var task: Task {
        switch self {
        case let .albumSearch(key):
            return .requestParameters(parameters: [ "method": "album.search",
                                                    "album": key,
                                                    "api_key": AlbumApi.apiKey,
                                                    "format": "json"],
                                      encoding: URLEncoding.default)
        }
    }

    var path: String {
        switch self {
        case .albumSearch:
            return "/2.0/"
        }
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String : String]? {
        return nil
    }

    private static let apiKey: String = "9dc53723e6b2654562dbcdcfcfac5112"
}
