//
//  ApiError.swift
//  LastFM
//
//  Created by Javier Flores Font on 21/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import Foundation

struct ApiError: Error, Decodable, Equatable {
    let message: String
    let error: Int

    static let generic = ApiError(message: "Something.went.wrong...".localized, error: 1)
}
