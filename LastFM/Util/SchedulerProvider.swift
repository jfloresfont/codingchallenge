import RxSwift

protocol SchedulerProtocol {
    var mainScheduler: SchedulerType {get}
    var backgroundScheduler: SchedulerType {get}
}

final class SchedulerProvider: SchedulerProtocol {
    var mainScheduler: SchedulerType = MainScheduler.instance
    var backgroundScheduler: SchedulerType = ConcurrentDispatchQueueScheduler(qos: .background)
}
