//
//  Inject.swift
//  LastFM
//
//  Created by Javier Flores Font on 21/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import Swinject

func inject<T>(_ type: T.Type) -> T {
    return Container.sharedContainer.resolve(type)!
}
