//
//  ImageDownloader.swift
//  LastFM
//
//  Created by Javier Flores Font on 22/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import Foundation
import RxSwift

protocol ImageDownloaderType {
    func get(urlString: String?) -> Observable<UIImage?>
}

struct ImageDownloader: ImageDownloaderType {
    let urlSession: URLSession

    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }

    func get(urlString: String?) -> Observable<UIImage?> {
        guard let urlString = urlString, let url = URL(string: urlString) else {
            return .just(nil)
        }
        return urlSession.rx.response(request: URLRequest(url: url)).map { response, data -> UIImage? in
            return UIImage(data: data)
        }
    }
}
