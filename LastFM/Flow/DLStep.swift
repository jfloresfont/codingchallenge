import RxFlow

enum DLStep: Step {
    case albumList
    case albumDetail(album: Album)
}
