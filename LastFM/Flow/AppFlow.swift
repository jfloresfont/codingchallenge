import UIKit
import RxFlow

final class AppFlow: Flow {
    var root: Presentable {
        return window
    }

    private let window: UIWindow

    init(withWindow window: UIWindow) {
        self.window = window
    }

    func navigate(to step: Step) -> NextFlowItems {
        guard let step = step as? DLStep else { return .none }
        switch step {
        case .albumList:
            return navigationToSearchScreen()
        case let .albumDetail(album):
            return navigationToDetailScreen(album: album)
        }
    }

    private func navigationToSearchScreen() -> NextFlowItems {
        let searchViewController: SearchViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        let viewModel = SearchViewModel(input: (searchKey: searchViewController.searchKey,
                                                selectedAlbum: searchViewController.selectedAlbum),
                                        dependency: (albumService: inject(AlbumServiceType.self),
                                                     schedulerProvider: inject(SchedulerProtocol.self)))

        searchViewController.bind(viewModel: viewModel)

        let navigationController = UINavigationController(rootViewController: searchViewController)
        navigationController.navigationBar.prefersLargeTitles = true
        window.rootViewController = navigationController

        return .one(flowItem: NextFlowItem(nextPresentable: searchViewController, nextStepper: viewModel))
    }

    private func navigationToDetailScreen(album: Album) -> NextFlowItems {
        let detailViewController: DetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        let viewModel = DetailViewModel(album: album,
                                        imageDownloader: inject(ImageDownloaderType.self))

        detailViewController.bind(viewModel: viewModel)

        window.rootViewController?.show(detailViewController, sender: nil)

        return .one(flowItem: NextFlowItem(nextPresentable: detailViewController, nextStepper: viewModel))
    }
}

final class AppStepper: Stepper {
    init() {
        step.accept(DLStep.albumList)
    }
}
