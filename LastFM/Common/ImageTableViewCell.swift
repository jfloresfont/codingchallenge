//
//  ImageTableViewCell.swift
//  LastFM
//
//  Created by Javier Flores Font on 22/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import UIKit

final class ImageTableViewCell: UITableViewCell {
    static let identifier = "ImageTableViewCell"

    @IBOutlet private weak var customImageView: UIImageView!

    func set(image: UIImage?) {
        customImageView.image = image
    }
}
