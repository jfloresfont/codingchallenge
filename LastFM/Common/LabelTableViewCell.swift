//
//  AlbumCell.swift
//  LastFM
//
//  Created by Javier Flores Font on 22/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import UIKit

final class LabelTableViewCell: UITableViewCell {
    static let identifier = "LabelTableViewCell"

    @IBOutlet private weak var titleLabel: UILabel!

    func set(title: String) {
        titleLabel.text = title
    }
}
