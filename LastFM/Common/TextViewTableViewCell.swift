//
//  TextViewTableViewCell.swift
//  LastFM
//
//  Created by Javier Flores Font on 22/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import UIKit

final class TextViewTableViewCell: UITableViewCell {
    static let identifier = "TextViewTableViewCell"
    
    @IBOutlet private weak var textView: UITextView! {
        didSet {
            textView.isEditable = false
            textView.dataDetectorTypes = .link
        }
    }

    func set(title: String) {
        textView.text = title
    }
}
