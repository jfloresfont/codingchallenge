//
//  Album.swift
//  LastFM
//
//  Created by Javier Flores Font on 21/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import Foundation

struct Album: Decodable, Equatable {
    let name: String
    let artist: String
    let url: String
    let image: [AlbumImage]
}

extension Album {
    var largeSizeImageURLString: String? {
        return image.filter { $0.size == "large" }.first?.urlString
    }
}
