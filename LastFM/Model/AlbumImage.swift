//
//  Image.swift
//  LastFM
//
//  Created by Javier Flores Font on 22/10/2018.
//  Copyright © 2018 javierflores. All rights reserved.
//

import Foundation

struct AlbumImage: Equatable, Decodable {
    let size: String
    let urlString: String

    enum ImageLookupKeys: String, CodingKey {
        case size = "size"
        case urlString = "#text"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: ImageLookupKeys.self)
        size = try values.decode(String.self, forKey: .size)
        urlString = try values.decode(String.self, forKey: .urlString)
    }

    init(size: String, urlString: String) {
        self.size = size
        self.urlString = urlString
    }
}
